#ifndef __AWESOME_H
#define __AWESOME_H

#include "smartcore/smartmodulethreaded.h"
#include "smartcore/smartmodulefactory.h"

#include <QPluginLoader>

class AwesomeReader;
class QThread;

class AwesomeModule : public SmartModuleThreaded
{
    Q_OBJECT
public:
    AwesomeModule(SmartCore &smartcore, const QString& name, const QJsonValue& config);
    virtual ~AwesomeModule();

    void _initModule() override;

    void consumeAndProduce();

    void _initConsumerChannels(SmartPool &pool) override;
    void _startConsume() override;
    void consume();
    void _stopConsume() override;

    void _initProducerChannels(SmartPool &pool) override;
    void _startProduce() override;
    void produce();
    void _stopProduce() override;

    QJsonObject awesome(SmartCore*, const QJsonObject &parameter, libsmartcore::GenericAsyncCallBack, void*);

protected:
    int m_timeoutMs;

    Channel *m_chInString = nullptr;
    uint32_t m_chInString_consumerHandle = INVALID_CONSUMER_HANDLE;

    Channel *m_chOutString = nullptr;

    AwesomeReader *m_awesomeReader = nullptr;
    QThread *m_awesomeReaderThread = nullptr;
};

class AwesomeModuleFactory : public QObject, public ISmartModuleFactory
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID IID_smartmodulefactory FILE "metadata.json")
    Q_INTERFACES(ISmartModuleFactory)
public:
    AwesomeModuleFactory();
    SmartModule *createInstance(SmartCore &smartcore, const QString &name, QJsonValue config) const override;
};

#endif

