#include "awesomereader.h"

#include <QThread>
#include <QDateTime>
#include <QLoggingCategory>

Q_LOGGING_CATEGORY(awesomereadercat, "awesomereader")

AwesomeReader::AwesomeReader()
    :QObject()
{
    qCDebug(awesomereadercat) << "Reader constructed" ;
}

AwesomeReader::~AwesomeReader()
{
    qCDebug(awesomereadercat) << "Reader destructed" ;
}

void AwesomeReader::run()
{
    qCInfo(awesomereadercat) << "AwesomeReader has been started" ;
    for (;;)
    {
        if (QThread::currentThread()->isInterruptionRequested())
        {
            qCInfo(awesomereadercat) << "Stopping AwesomeReader..." ;
            QThread::currentThread()->quit();
            return;
        }

        QThread::msleep(100);

        QDateTime dt = QDateTime::currentDateTime();
        *m_writeTime = dt.toMSecsSinceEpoch();

        m_mutex.lock();
        uint64_t *tmp = m_readTime;
        m_readTime = m_writeTime;
        m_writeTime = tmp;
        m_mutex.unlock();
    }
}

uint64_t AwesomeReader::time()
{
    m_mutex.lock();
    uint64_t ret = *m_readTime;
    m_mutex.unlock();
    return ret;
}
