#include "awesome.h"
#include "awesomereader.h"

#include "smartcore/smartcore.h"
#include "smartcore/smartpool.h"
#include "smartcore/channel.h"
#include "smartcore/timeseries.h"
#include "smartcore/timestampedsample.h"

#include <QJsonArray>
#include <QJsonObject>
#include <QLoggingCategory>

#include <cmath>

Q_LOGGING_CATEGORY(awesomecat, "module.awesome")

const int DEFAULT_TIMEOUT_MS = 1000;

AwesomeModuleFactory::AwesomeModuleFactory()
    : ISmartModuleFactory()
{
}

SmartModule *AwesomeModuleFactory::createInstance(SmartCore &smartcore, const QString &name, QJsonValue config) const
{
    return new AwesomeModule(smartcore, name, config);
}

AwesomeModule::AwesomeModule(SmartCore &smartcore, const QString &name, const QJsonValue &config)
    : SmartModuleThreaded(smartcore,
                          name,
                          config,
                          libsmartcore::SmartModuleType::Consumer | libsmartcore::SmartModuleType::Producer,
                          std::bind(&AwesomeModule::consumeAndProduce, this),
                          QThread::NormalPriority,
                          {
                            {"awesome", std::bind(&AwesomeModule::awesome, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4)},
                          }
                          )
{
    qCDebug(awesomecat) << "Module constructed" ;
}

AwesomeModule::~AwesomeModule()
{
    if (m_awesomeReaderThread)
    {
        m_awesomeReaderThread->requestInterruption();
        m_awesomeReaderThread->wait();
        qCInfo(awesomecat) << "AwesomeReader has been stopped" ;
    }
    if (m_awesomeReader)
        m_awesomeReader->deleteLater();

    if (m_chInString_consumerHandle!=INVALID_CONSUMER_HANDLE)
    {
        if (m_chInString)
        {
            Timeseries *timeseries = m_chInString->timeseries();
            timeseries->deregisterConsumer(m_chInString_consumerHandle);
        }
    }
    qCDebug(awesomecat) << "Module destructed" ;
}

void AwesomeModule::_initModule()
{
    qCDebug(awesomecat) << "Module initiated" ;

    bool ok;
    QJsonObject joConfig = this->config();
    //  timeoutMs
    m_timeoutMs = joConfig["timeoutMs"].toVariant().toInt(&ok);
    if (!ok || m_timeoutMs<1)
        m_timeoutMs = DEFAULT_TIMEOUT_MS;
    qCDebug(awesomecat) << "timeoutMs = " << m_timeoutMs;
    //
    //  greeting
    QString greeting = joConfig["greeting"].toString();
    qCDebug(awesomecat) << "greeting = " << greeting ;
    //
    //  vector
    QJsonObject joVector = joConfig["vector"].toObject();
    qCDebug(awesomecat) << "vector   = " << joVector ;
    QVariantMap vmVector = joVector.toVariantMap();
    QVariant vX = vmVector["x"];
    double x = vX.toDouble();
    double y = vmVector["y"].toDouble();
    double z = vmVector["z"].toDouble();
    qCInfo(awesomecat) << "| vector | = " << pow(x*x+y*y+z*z, 0.5);
    //
    //  numbers
    QJsonArray jaNumbers = joConfig["numbers"].toArray();
    qCDebug(awesomecat) << "numbers  = " << jaNumbers ;
    QVariantList vlNumbers = jaNumbers.toVariantList();
    double sumOfNumbers = 0.0;
    for (QVariant vNumber : vlNumbers)
        sumOfNumbers += vNumber.toDouble();
    qCInfo(awesomecat) << "sumOfNumbers = " << sumOfNumbers;

    //  AwesomeReader
    m_awesomeReader = new AwesomeReader;
    m_awesomeReaderThread = new QThread(this);
    m_awesomeReader->moveToThread(m_awesomeReaderThread);
    m_awesomeReaderThread->start();
    QObject obj;
    connect(&obj, &QObject::destroyed, m_awesomeReader, &AwesomeReader::run);

    emit moduleInitiated(true);
}

void AwesomeModule::consumeAndProduce()
{
    consume();
    produce();
}

void AwesomeModule::_initConsumerChannels(SmartPool &pool)
{
    m_chInString = pool.channelByName("chInString");
    if (!m_chInString)
    {
        qCWarning(awesomecat) << "No channel chInString found within pool!" ;
        emit consumerChannelInitiated(false);
        return;
    }
    emit consumerChannelInitiated(true);
}

void AwesomeModule::_startConsume()
{
    Timeseries *timeseries = m_chInString->timeseries();
    m_chInString_consumerHandle = timeseries->registerConsumer();
    emit consumeStarted();
}

void AwesomeModule::consume()
{
    QVector<TimestampedSample<QString>> buf;
    buf.resize(1);
    Timeseries *timeseries = m_chInString->timeseries();
    libsmartcore::ConsumeResult result = timeseries->consume(m_chInString_consumerHandle, buf.data(), buf.size());
    if (result.result==libsmartcore::ConsumeResult::Result::OK)
    {
        TimestampedSample<QString> sample = buf[0];
        qCInfo(awesomecat) << "sample consumed : ts =" << QString::number(sample.ts) << "; value =" << sample.value;
    }
}

void AwesomeModule::_stopConsume()
{
    Timeseries *timeseries = m_chInString->timeseries();
    timeseries->deregisterConsumer(m_chInString_consumerHandle);
    m_chInString_consumerHandle = INVALID_CONSUMER_HANDLE;
    emit consumeStopped();
}

void AwesomeModule::_initProducerChannels(SmartPool &pool)
{
    libsmartcore::AddChannelResult result = pool.addChannel("chOutString", libsmartcore::DataType::String, 1, "");
    m_chOutString = result.channel;
    emit producerChannelInitiated(result.result);
}

void AwesomeModule::_startProduce()
{
    startTimer(m_timeoutMs);
    emit produceStarted();
}

void AwesomeModule::produce()
{
    static uint64_t lastTime = 0;
    uint64_t time = m_awesomeReader->time();
    uint64_t dtime = time - lastTime;
    lastTime = time;

    TimestampedSample<QString> sample;
    sample.ts = smartcore().currentTimestamp();
    sample.value = QString("Timestamp/Timediff: %1 / %2").arg(sample.ts).arg(dtime);
    QVector<TimestampedSample<QString>> buf = { sample };
    Timeseries *timeseries = m_chOutString->timeseries();
    timeseries->produce(buf.data(), buf.size());
}

void AwesomeModule::_stopProduce()
{
    stopTimer();
    emit produceStopped();
}

QJsonObject AwesomeModule::awesome(SmartCore*, const QJsonObject &parameter, libsmartcore::GenericAsyncCallBack, void*)
{
    qCInfo(awesomecat) << "Call-Interface function awesome called, param = " << parameter["param"].toString();
    QJsonObject ret;
    ret.insert("result", 0);
    return ret;
}
