#ifndef __AWESOMEREADER_H
#define __AWESOMEREADER_H

#include <QObject>
#include <QMutex>

class AwesomeReader : public QObject
{
    Q_OBJECT
public:
    AwesomeReader();
    virtual ~AwesomeReader();

    void run();

    uint64_t time();

protected:
    QMutex m_mutex;
    uint64_t m_time0 = 0;
    uint64_t m_time1 = 0;
    uint64_t *m_readTime = &m_time0;
    uint64_t *m_writeTime = &m_time1;
};

#endif
