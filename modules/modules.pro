TEMPLATE = subdirs

include(../common.pri)

SUBDIRS = \
    awesome \
    producermodule \
    performanceconsumer \
    performanceproducer \
    configautoload \
    functiongenerator \
    opticloud \
    opticloudconfig \
    modbus \
    candbc \
    mvb \
    alarmmodule \
    relaymodule \
    alarmconsumer \
    osfwriter \
    iso8573 \
    scpi \
    bekobdlsystem \
    cpuload \
    procreader \
    hirestest \
    calc_example1 \
    trelleborg \
    zsg \
    helms \
    terminal15 \
    memoryload \
    mtu \
    geofencing \
    osfudpstreamer \
    calc \
    em2000 \
    batmon \
    fmudp \
    fmproducer \
    coresensing \
    j1587 \
    fmconsumer \
    odometer \
    canbus \
    dbc \
    hwio \
    smartmvb \
    notifier \
    uptime \
    almemo \
    calc2 \

config_gpsd {
    SUBDIRS += \
        gpsd \
}

versionAtLeast(QT_VERSION, 6.0.0) {
} else {
    SUBDIRS += \
        gps \
}

enable_grpc_module {
    message("Enabled build grpc_module")
    SUBDIRS += grpc_module
}

# extra target for SDK packaging
sdkinstalltarget.target = sdkinstall
sdkinstalltarget.commands += mkdir -p $(SDKPREFIX)
sdkinstalltarget.commands += && mkdir -p $(SDKPREFIX)/plugins
PLUGIN_LIST += functiongenerator
PLUGIN_LIST += osfwriter
PLUGIN_LIST += opticloud
for (PLUGIN, PLUGIN_LIST) {
    sdkinstalltarget.commands += && cp -f -P $$OUT_PWD"/"$$PLUGIN"/lib*so*" $(SDKPREFIX)/plugins
}
QMAKE_EXTRA_TARGETS += sdkinstalltarget

