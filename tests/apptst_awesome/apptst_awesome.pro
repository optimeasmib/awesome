TEMPLATE = app

QT -= gui
QT += core testlib scxml

include(../../common.pri)

SOURCES = tst_awesome.cpp

INCLUDEPATH += "../../libsmartcore/"
LIBS += -L../../libsmartcore -lsmartcore

DESTDIR = ../../smartcore
QMAKE_POST_LINK += $$QMAKE_COPY $$quote($$PWD/tst_awesome.json) $$quote($$DESTDIR) $$escape_expand(\\n\\t)

OTHER_FILES += tst_awesome.json

# install
defined(INSTALL_TARGET, var) {
    equals(INSTALL_TARGET, "system") {
        target.path = $$[QT_INSTALL_TEST]
    } else {
        target.path = $$INSTALL_TARGET/smartcore
    }
} else {
    target.path = /sdi/apps/$$APPNAME
}
json.path = $$target.path
json.files += tst_awesome.json
INSTALLS += target json

