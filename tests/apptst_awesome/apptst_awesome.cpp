#include "smartcore.h"

#include <QTest>
#include <QSignalSpy>

class AwesomeModuleTest : public QObject
{
    Q_OBJECT
private slots:
    void initTestCase();
    void cleanupTestCase();

    void awesomeModuleTest();
private:
    QScopedPointer<SmartCore> m_smartcore;
};

void AwesomeModuleTest::initTestCase()
{
    m_smartcore.reset(new SmartCore());
}

void AwesomeModuleTest::cleanupTestCase()
{
    m_smartcore.reset(nullptr);
}

void AwesomeModuleTest::awesomeModuleTest()
{
    QSignalSpy readySpy(m_smartcore.get(),&SmartCore::ready);
    QEventLoop eventLoop;
    eventLoop.processEvents();
    m_smartcore->loadMeasurementConfig("./tst_awesome.json", true);
    int totalRunningTimeMs = 10000;
    while (totalRunningTimeMs > 0) {
        totalRunningTimeMs -= 100;
        QTest::qSleep(100);
        eventLoop.processEvents();
    }
    m_smartcore->stop();
    while (readySpy.count()<2)
        eventLoop.processEvents();
}

QTEST_MAIN(AwesomeModuleTest)
#include "tst_awesome.moc"

